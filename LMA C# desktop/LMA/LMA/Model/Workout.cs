﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Model
{
    public class Workout
    {
        public virtual int Id { get; protected set; }
        public virtual string name { get; set; }
        public virtual string description { get; set; }
        public virtual string category { get; set; }
        public virtual string link { get; set; }
        public virtual int reps { get; set; }
        public virtual double time { get; set; }

        public Workout()
        {
        }

        public Workout(string name, string description, string category, string link, int reps)
        {
            this.name = name;
            this.description = description;
            this.category = category;
            this.link = link;
            this.reps = reps;
        }
    }
}

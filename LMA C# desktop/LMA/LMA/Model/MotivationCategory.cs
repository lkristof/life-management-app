﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Model
{
    public class MotivationCategory
    {
        public virtual int Id { get; protected set; }
        public virtual string name { get; set; }

        public MotivationCategory()
        {
        }

        public MotivationCategory(string name)
        {
            this.name = name;
        }

        public override bool Equals(object obj)
        {
            try
            {
                MotivationCategory motivationCategory = (MotivationCategory)obj;
                return name.Equals(motivationCategory.name);
            } catch (Exception e)
            {
                return false;
            }
            
        }

        public override string ToString()
        {
            return name;
        }
    }
}

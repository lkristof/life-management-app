﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Model
{
    public class Sleep
    {
        public virtual int Id { get; protected set; }
        public virtual DateTime date { get; set; }
        public virtual string sleepStart { get; set; }
        public virtual string sleepEnd { get; set; }
        public virtual int rating { get; set; }

        public Sleep()
        {
        }

        public Sleep(DateTime date, string sleepStart, string sleepEnd, int rating)
        {
            this.date = date;
            this.sleepStart = sleepStart;
            this.sleepEnd = sleepEnd;
            this.rating = rating;
        }

        public override string ToString()
        {
            return date.Date.ToString() + " ---- " + sleepStart + " until " + sleepEnd + " --- rate: " + rating;
        }
    }
}

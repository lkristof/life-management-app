﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Model.RepositoryInterface
{
    public interface ITaskRepository
    {
        List<Task> getAllTasks();
        void addTask(Task task);
        List<Task> getTasksByDate(DateTime dateTime);
    }
}

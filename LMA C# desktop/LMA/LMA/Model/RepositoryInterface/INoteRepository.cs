﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Model.RepositoryInterface
{
    public interface INoteRepository
    {
        List<Note> getAllNotes();
        void addNote(Note note);
        List<Note> getNotesByDate(DateTime dateTime);
    }
}

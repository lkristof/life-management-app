﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Model.RepositoryInterface
{
    public interface IMealRepository
    {
        List<Meal> getAllMeals();
        void addMeal(Meal meal);
        List<Meal> getMealsByDate(DateTime dateTime);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Model.RepositoryInterface
{
    public interface IWorkoutRepository
    {
        List<Workout> getAllWorkouts();
        void addWorkout(Workout workout);
        List<Workout> getWorkoutsByCategory(string category);
    }
}

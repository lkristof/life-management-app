﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Model.RepositoryInterface
{
    public interface IGroceryRepository
    {
        List<Grocery> getAllGroceries();
        void addGrocery(Grocery grocery);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Model.RepositoryInterface
{
    public interface IMotivationRepository
    {
        List<Motivation> getAllMotivations();
        void addMotivation(Motivation motivation);
        void deleteMotivation(Motivation motivation);
        void deleteAllMotivations(List<Motivation> motivations);
        List<Motivation> getMotivationsByCategory(MotivationCategory motivationCategory);
    }
}

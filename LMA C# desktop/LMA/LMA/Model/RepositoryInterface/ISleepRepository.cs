﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Model.RepositoryInterface
{
    public interface ISleepRepository
    {
        List<Sleep> getAllSleeps();
        void addSleep(Sleep sleep);
        Sleep getSleepByDate(DateTime dateTime);
    }
}

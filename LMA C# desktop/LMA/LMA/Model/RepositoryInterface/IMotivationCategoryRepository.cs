﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Model.RepositoryInterface
{
    public interface IMotivationCategoryRepository
    {
        List<MotivationCategory> getAllMotivations();
        void addMotivationCategory(MotivationCategory motivationCategory);
    }
}

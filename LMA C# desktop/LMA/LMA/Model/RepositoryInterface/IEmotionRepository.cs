﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Model.RepositoryInterface
{
    public interface IEmotionRepository
    {
        IList<Emotion> getAllEmotions();
        void addEmotion(Emotion emotion);
    }
}

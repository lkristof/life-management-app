﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Model.Mappings
{
    class MotivationMap : ClassMap<Motivation>
    {
        public MotivationMap()
        {
            Id(x => x.Id);
            Map(x => x.note);
            References(x => x.category);
        }
    }
}

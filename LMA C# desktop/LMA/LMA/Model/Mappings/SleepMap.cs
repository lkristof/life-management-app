﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Model.Mappings
{
    class SleepMap : ClassMap<Sleep>
    {
        public SleepMap()
        {
            Id(x => x.Id);
            Map(x => x.date);
            Map(x => x.sleepStart);
            Map(x => x.sleepEnd);
            Map(x => x.rating);
        }
    }
}

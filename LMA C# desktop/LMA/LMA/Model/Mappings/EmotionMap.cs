﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Model.Mappings
{
    class EmotionMap : ClassMap<Emotion>
    {
        public EmotionMap()
        {
            Id(x => x.Id);
            Map(x => x.name);
        }
    }
}

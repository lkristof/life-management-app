﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Model.Mappings
{
    class WorkoutMap : ClassMap<Workout>
    {
        public WorkoutMap()
        {
            Id(x => x.Id);
            Map(x => x.name);
            Map(x => x.description);
            Map(x => x.reps);
            Map(x => x.category);
            Map(x => x.link);
        }
    }
}

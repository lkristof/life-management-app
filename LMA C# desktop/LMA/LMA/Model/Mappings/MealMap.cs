﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Model.Mappings
{
    class MealMap : ClassMap<Meal>
    {
        public MealMap()
        {
            Id(x => x.Id);
            Map(x => x.name);
            Map(x => x.dateTime);
            HasManyToMany(x => x.groceries).Table("MealGrocerie");
        }
    }
}

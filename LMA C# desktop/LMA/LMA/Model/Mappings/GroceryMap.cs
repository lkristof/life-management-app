﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Model.Mappings
{
    class GroceryMap : ClassMap<Grocery>
    {
        public GroceryMap()
        {
            Id(x => x.Id);
            Map(x => x.name);
            Map(x => x.amount);
            Map(x => x.fats);
            Map(x => x.carbs);
            Map(x => x.proteins);
        }
    }
}

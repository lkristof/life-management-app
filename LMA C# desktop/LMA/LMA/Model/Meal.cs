﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Model
{
    public class Meal
    {
        public virtual int Id { get; protected set; }
        public virtual DateTime dateTime { get; set; }
        public virtual string name { get; set; }
        public virtual IList<Grocery> groceries { get; set; }

        public Meal()
        {
            groceries = new List<Grocery>();
        }

        public Meal(DateTime dateTime, string name, IList<Grocery> groceries)
        {
            this.dateTime = dateTime;
            this.name = name;
            this.groceries = groceries;
        }

        public override string ToString()
        {
            return name + " ---- " + dateTime + " --- Food: " + groceries.ToString();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Model
{
    public class Task
    {
        public virtual int Id { get; protected set; }
        public virtual string name { get; set; }
        public virtual DateTime dateTime { get; set; }
        public virtual string description { get; set; }

        public Task() { }

        public Task(string name, DateTime dateTime, string description)
        {
            this.name = name;
            this.dateTime = dateTime;
            this.description = description;
        }

        public override string ToString()
        {
            return name + " --- " + dateTime + " --- " + description;
        }
    }
}

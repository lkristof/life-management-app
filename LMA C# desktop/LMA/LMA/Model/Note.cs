﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Model
{
    public class Note
    {
        public virtual int Id { get; protected set; }
        public virtual DateTime dateTime { get; set; }
        public virtual string description { get; set; }
        public virtual Emotion emotion { get; set; }

        public Note()
        {
        }

        public Note(DateTime dateTime, string description)
        {
            this.dateTime = dateTime;
            this.description = description;
        }
    }
}

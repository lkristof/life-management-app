﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Model
{
    public class Grocery
    {
        public virtual int Id { get; protected set; }
        public virtual string name { get; set; }
        public virtual double amount { get; set; }
        public virtual double calories { get; set; }
        public virtual double fats { get; set; }
        public virtual double carbs { get; set; }
        public virtual double proteins { get; set; }

        public Grocery()
        {
        }

        public Grocery(string text, double amount, double calories, double fats, double carbs, double proteins)
        {
            name = text;
            this.amount = amount;
            this.calories = calories;
            this.fats = fats;
            this.carbs = carbs;
            this.proteins = proteins;
        }

        public override string ToString()
        {
            return name + " with calories: " + amount;
        }
    }
}

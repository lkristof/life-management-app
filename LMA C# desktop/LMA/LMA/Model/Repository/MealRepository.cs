﻿using LMA.Model.RepositoryInterface;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Model.Repository
{
    public class MealRepository : IMealRepository
    {
        ISession session;

        public MealRepository(ISession session)
        {
            this.session = session;
        }

        public void addMeal(Meal meal)
        {
            var transaction = session.BeginTransaction();
            session.Save(meal);
            transaction.Commit();
            session.Flush();
        }

        public List<Meal> getAllMeals()
        {
            var transaction = session.BeginTransaction();
            return session.Query<Meal>().ToList();
        }

        public List<Meal> getMealsByDate(DateTime dateTime)
        {
            var transaction = session.BeginTransaction();
            List<Meal> listOfAll = session.Query<Meal>().ToList();
            List<Meal> listForReturn = new List<Meal>();
            foreach (Meal meal in listOfAll)
            {
                if (meal.dateTime.Date.Equals(dateTime.Date))
                    listForReturn.Add(meal);
            }
            return listForReturn;
        }
    }
}

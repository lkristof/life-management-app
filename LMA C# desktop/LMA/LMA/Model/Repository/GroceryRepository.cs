﻿using LMA.Model.RepositoryInterface;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Model.Repository
{
    public class GroceryRepository : IGroceryRepository
    {
        ISession session;

        public GroceryRepository(ISession session)
        {
            this.session = session;
        }

        public void addGrocery(Grocery grocery)
        {
            var transaction = session.BeginTransaction();
            session.Save(grocery);
            transaction.Commit();
            session.Flush();
        }

        public List<Grocery> getAllGroceries()
        {
            
            return session.Query<Grocery>().ToList();
            
        }
    }
}

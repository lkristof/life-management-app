﻿using LMA.Model.RepositoryInterface;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Model.Repository
{
    public class MotivationRepository : IMotivationRepository
    {
        ISession session;

        public MotivationRepository(ISession session)
        {
            this.session = session;
        }

        public void addMotivation(Motivation motivation)
        {
            var transaction = session.BeginTransaction();
            session.Save(motivation);
            transaction.Commit();
            session.Flush();
        }

        public void deleteAllMotivations(List<Motivation> motivations)
        {
            var transaction = session.BeginTransaction();
            foreach(Motivation motivation in motivations)
            {
                session.Delete(motivation);
                transaction.Commit();
            }
            session.Flush();
        }

        public void deleteMotivation(Motivation motivation)
        {
            var transaction = session.BeginTransaction();
            session.Delete(motivation);
            transaction.Commit();
            session.Flush();
        }

        public List<Motivation> getAllMotivations()
        {
            
            return session.Query<Motivation>().ToList();
        }

        public List<Motivation> getMotivationsByCategory(MotivationCategory motivationCategory)
        {
            
            List<Motivation> listOfAllMotivations = session.Query<Motivation>().ToList();
            List<Motivation> listForReturn = new List<Motivation>();
            foreach (Motivation motivation in listOfAllMotivations)
                if (motivation.category.Equals(motivationCategory))
                    listForReturn.Add(motivation);
            return listForReturn;
        }
    }
}

﻿using LMA.Model.RepositoryInterface;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Model.Repository
{
    public class NoteRepository : INoteRepository
    {
        ISession session;

        public NoteRepository(ISession session)
        {
            this.session = session;
        }

        public void addNote(Note note)
        {
            var transaction = session.BeginTransaction();
            session.Save(note);
            transaction.Commit();
            session.Flush();
        }

        public List<Note> getAllNotes()
        {
            var transaction = session.BeginTransaction();
            return session.Query<Note>().ToList();
        }

        public List<Note> getNotesByDate(DateTime dateTime)
        {
            var transaction = session.BeginTransaction();
            List<Note> listOfAll = session.Query<Note>().ToList();
            List<Note> listForReturn = new List<Note>();
            foreach (Note note in listOfAll)
            {
                if (note.dateTime.Date.Equals(dateTime.Date))
                    listForReturn.Add(note);
            }
            return listForReturn;
        }
    }
}

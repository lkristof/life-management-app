﻿using LMA.Model.RepositoryInterface;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Model.Repository
{
    public class TaskRepository : ITaskRepository
    {
        ISession session;

        public TaskRepository(ISession session)
        {
            this.session = session;
        }

        public void addTask(Task task)
        {
            var transaction = session.BeginTransaction();
            session.Save(task);
            transaction.Commit();
            session.Flush();
        }

        public List<Task> getAllTasks()
        {
            var transaction = session.BeginTransaction();
            return session.Query<Task>().ToList();
        }

        public List<Task> getTasksByDate(DateTime dateTime)
        {
            var transaction = session.BeginTransaction();
            List<Task> listOfAll = session.Query<Task>().ToList();
            List<Task> listForReturn = new List<Task>();
            foreach(Task task in listOfAll)
            {
                if (task.dateTime.Date.Equals(dateTime.Date))
                    listForReturn.Add(task);
            }
            return listForReturn;
        }
    }
}

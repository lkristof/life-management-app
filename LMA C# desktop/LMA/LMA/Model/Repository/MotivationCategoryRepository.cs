﻿using LMA.Model.RepositoryInterface;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Model.Repository
{
    public class MotivationCategoryRepository : IMotivationCategoryRepository
    {
        ISession session;

        public MotivationCategoryRepository(ISession session)
        {
            this.session = session;
        }

        public void addMotivationCategory(MotivationCategory motivationCategory)
        {
            var transaction = session.BeginTransaction();
            session.Save(motivationCategory);
            transaction.Commit();
            session.Flush();
        }

        public List<MotivationCategory> getAllMotivations()
        {
            return session.Query<MotivationCategory>().ToList();
        }
    }
}

﻿using LMA.Model.RepositoryInterface;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Model.Repository
{
    class WorkoutRepository : IWorkoutRepository
    {
        ISession session;

        public WorkoutRepository(ISession session)
        {
            this.session = session;
        }

        public void addWorkout(Workout workout)
        {
            var transaction = session.BeginTransaction();
            session.Save(workout);
            transaction.Commit();
            session.Flush();
        }

        public List<Workout> getAllWorkouts()
        {
            var transaction = session.BeginTransaction();
            return session.Query<Workout>().ToList();
        }

        public List<Workout> getWorkoutsByCategory(string category)
        {
            var transaction = session.BeginTransaction();
            List<Workout> listOfAll = session.Query<Workout>().ToList();
            List<Workout> listForReturn = new List<Workout>();
            foreach (Workout workout in listOfAll)
                if (workout.category.Equals(category))
                    listForReturn.Add(workout);
            return listForReturn;
        }
    }
}

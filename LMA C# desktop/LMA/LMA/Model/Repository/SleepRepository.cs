﻿using LMA.Model.RepositoryInterface;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Model.Repository
{
    public class SleepRepository : ISleepRepository
    {
        ISession session;

        public SleepRepository(ISession session)
        {
            this.session = session;
        }

        public void addSleep(Sleep sleep)
        {
            var transaction = session.BeginTransaction();
            session.Save(sleep);
            transaction.Commit();
            session.Flush();
        }

        public List<Sleep> getAllSleeps()
        {
            var transaction = session.BeginTransaction();
            return session.Query<Sleep>().ToList();
        }

        public Sleep getSleepByDate(DateTime dateTime)
        {
            var transaction = session.BeginTransaction();
            List<Sleep> listOfAll = session.Query<Sleep>().ToList();
            List<Sleep> listForReturn = new List<Sleep>();
            foreach (Sleep sleep in listOfAll)
            {
                if (sleep.date.Date.Equals(dateTime.Date))
                    listForReturn.Add(sleep);
            }
            if (listForReturn.Count == 0)
                return null;
            return listForReturn.First();
        }
    }
}

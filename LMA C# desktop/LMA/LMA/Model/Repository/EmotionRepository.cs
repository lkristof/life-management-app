﻿using LMA.Model.RepositoryInterface;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Model.Repository
{
    public class EmotionRepository : IEmotionRepository
    {
        ISession session;

        EmotionRepository(ISession session)
        {
            this.session = session;
        }

        public void addEmotion(Emotion emotion)
        {
            var transaction = session.BeginTransaction();
            session.Save(emotion);
            transaction.Commit();
            session.Flush();
        }

        public IList<Emotion> getAllEmotions()
        {
            var transaction = session.BeginTransaction();
            return session.Query<Emotion>().ToList();
        }
    }
}

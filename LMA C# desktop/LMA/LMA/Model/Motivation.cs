﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Model
{
    public class Motivation
    {
        public virtual int Id { get; protected set; }
        public virtual string note { get; set; }
        public virtual MotivationCategory category { get; set; }

        public Motivation()
        {
        }

        public Motivation(string note, MotivationCategory category)
        {
            this.note = note;
            this.category = category;
        }

        public override string ToString()
        {
            return note;
        }
    }
}

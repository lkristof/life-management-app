﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using LMA.Model;
using LMA.Model.Repository;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LMA
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
           
            using (var session = NHibernateService.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var emotio = new Emotion { name = "Sad" };
                    session.Save(emotio);
                    

                    var training1 = new Workout("Lifts", "abde", "A", "Ajdosde", 15);
                    var training2 = new Workout("Deads", "abde", "A", "Ajdosde", 15);
                    var training3 = new Workout("Splits", "abde", "A", "Ajdosde", 15);
                    session.Save(training1);
                    
                    session.Save(training2);
                    
                    session.Save(training3);

                    var motCategory1 = new MotivationCategory("Exams");
                    var motCategory2 = new MotivationCategory("Workout");
                    session.Save(motCategory1);
                    session.Save(motCategory2);

                    var mot1 = new Motivation("A", motCategory1);
                    var mot2 = new Motivation("B", motCategory1);
                    var mot3 = new Motivation("C", motCategory1);
                    var mot4 = new Motivation("D", motCategory2);

                    session.Save(mot1);
                    session.Save(mot2);
                    session.Save(mot3);
                    session.Save(mot4);
                    transaction.Commit();

                }
            }
                    Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new LMA());
        }
        
    }
}

﻿using LMA.Controller;
using LMA.Controller.ControllerInterface;
using LMA.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LMA
{
    public partial class PersonalPlanner : Form
    {
        ITaskController taskController;

        public PersonalPlanner()
        {
            InitializeComponent();
            // Define the border style of the form to a dialog box.
            this.FormBorderStyle = FormBorderStyle.FixedDialog;

            // Set the MaximizeBox to false to remove the maximize box.
            this.MaximizeBox = false;

            // Set the MinimizeBox to false to remove the minimize box.
            this.MinimizeBox = false;

            // Set the start position of the form to the center of the screen.
            this.StartPosition = FormStartPosition.CenterScreen;

            taskController = new TaskController(this);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PlanCreateForm planCreateForm = new PlanCreateForm(taskController);
            planCreateForm.Show();
        }

        private void PersonalPlanner_Load(object sender, EventArgs e)
        {

        }

        private void PlansExtractButton_Click(object sender, EventArgs e)
        {
            taskController.presentTasks();
        }
    }
}

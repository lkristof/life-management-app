﻿namespace LMA.View
{
    partial class MotivationsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxCategory = new System.Windows.Forms.ComboBox();
            this.buttonFetchMotivation = new System.Windows.Forms.Button();
            this.checkedListBoxMotivation = new System.Windows.Forms.CheckedListBox();
            this.buttonDislike = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // comboBoxCategory
            // 
            this.comboBoxCategory.FormattingEnabled = true;
            this.comboBoxCategory.Location = new System.Drawing.Point(13, 13);
            this.comboBoxCategory.Name = "comboBoxCategory";
            this.comboBoxCategory.Size = new System.Drawing.Size(177, 21);
            this.comboBoxCategory.TabIndex = 0;
            // 
            // buttonFetchMotivation
            // 
            this.buttonFetchMotivation.Location = new System.Drawing.Point(209, 10);
            this.buttonFetchMotivation.Name = "buttonFetchMotivation";
            this.buttonFetchMotivation.Size = new System.Drawing.Size(110, 24);
            this.buttonFetchMotivation.TabIndex = 1;
            this.buttonFetchMotivation.Text = "Fetch";
            this.buttonFetchMotivation.UseVisualStyleBackColor = true;
            this.buttonFetchMotivation.Click += new System.EventHandler(this.buttonFetchMotivation_Click);
            // 
            // checkedListBoxMotivation
            // 
            this.checkedListBoxMotivation.FormattingEnabled = true;
            this.checkedListBoxMotivation.Location = new System.Drawing.Point(13, 53);
            this.checkedListBoxMotivation.Name = "checkedListBoxMotivation";
            this.checkedListBoxMotivation.Size = new System.Drawing.Size(306, 334);
            this.checkedListBoxMotivation.TabIndex = 2;
            // 
            // buttonDislike
            // 
            this.buttonDislike.Location = new System.Drawing.Point(12, 393);
            this.buttonDislike.Name = "buttonDislike";
            this.buttonDislike.Size = new System.Drawing.Size(307, 45);
            this.buttonDislike.TabIndex = 3;
            this.buttonDislike.Text = "Dislike";
            this.buttonDislike.UseVisualStyleBackColor = true;
            this.buttonDislike.Click += new System.EventHandler(this.buttonDislike_Click);
            // 
            // MotivationsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::LMA.Properties.Resources.light_bulb_1246043_1920;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonDislike);
            this.Controls.Add(this.checkedListBoxMotivation);
            this.Controls.Add(this.buttonFetchMotivation);
            this.Controls.Add(this.comboBoxCategory);
            this.Name = "MotivationsForm";
            this.Text = "MotivationsForm";
            this.Load += new System.EventHandler(this.MotivationsForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxCategory;
        private System.Windows.Forms.Button buttonFetchMotivation;
        private System.Windows.Forms.CheckedListBox checkedListBoxMotivation;
        private System.Windows.Forms.Button buttonDislike;
    }
}
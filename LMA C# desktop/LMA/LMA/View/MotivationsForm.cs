﻿using LMA.Controller;
using LMA.Controller.ControllerInterface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LMA.View
{
    public partial class MotivationsForm : Form
    {
        IMotivationController motivationController;

        public MotivationsForm()
        {
            InitializeComponent();

            // Define the border style of the form to a dialog box.
            this.FormBorderStyle = FormBorderStyle.FixedDialog;

            // Set the MaximizeBox to false to remove the maximize box.
            this.MaximizeBox = false;

            // Set the MinimizeBox to false to remove the minimize box.
            this.MinimizeBox = false;
            motivationController = new MotivationController(this);
            motivationController.initForm();
        }

        private void MotivationsForm_Load(object sender, EventArgs e)
        {

        }

        private void buttonFetchMotivation_Click(object sender, EventArgs e)
        {
            motivationController.onFetch();
        }

        private void buttonDislike_Click(object sender, EventArgs e)
        {
            motivationController.dislike();
        }
    }
}

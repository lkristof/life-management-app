﻿namespace LMA.View
{
    partial class EmotionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dateTimePickerEmotion = new System.Windows.Forms.DateTimePicker();
            this.buttonFetchDiary = new System.Windows.Forms.Button();
            this.listViewEmotions = new System.Windows.Forms.ListView();
            this.buttonAddNote = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // dateTimePickerEmotion
            // 
            this.dateTimePickerEmotion.Location = new System.Drawing.Point(13, 13);
            this.dateTimePickerEmotion.Name = "dateTimePickerEmotion";
            this.dateTimePickerEmotion.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerEmotion.TabIndex = 0;
            // 
            // buttonFetchDiary
            // 
            this.buttonFetchDiary.Location = new System.Drawing.Point(13, 57);
            this.buttonFetchDiary.Name = "buttonFetchDiary";
            this.buttonFetchDiary.Size = new System.Drawing.Size(200, 25);
            this.buttonFetchDiary.TabIndex = 1;
            this.buttonFetchDiary.Text = "Fetch";
            this.buttonFetchDiary.UseVisualStyleBackColor = true;
            this.buttonFetchDiary.Click += new System.EventHandler(this.buttonFetchDiary_Click);
            // 
            // listViewEmotions
            // 
            this.listViewEmotions.Location = new System.Drawing.Point(13, 107);
            this.listViewEmotions.Name = "listViewEmotions";
            this.listViewEmotions.Size = new System.Drawing.Size(200, 331);
            this.listViewEmotions.TabIndex = 2;
            this.listViewEmotions.UseCompatibleStateImageBehavior = false;
            // 
            // buttonAddNote
            // 
            this.buttonAddNote.Location = new System.Drawing.Point(308, 382);
            this.buttonAddNote.Name = "buttonAddNote";
            this.buttonAddNote.Size = new System.Drawing.Size(152, 56);
            this.buttonAddNote.TabIndex = 4;
            this.buttonAddNote.Text = "Add note";
            this.buttonAddNote.UseVisualStyleBackColor = true;
            this.buttonAddNote.Click += new System.EventHandler(this.buttonAddNote_Click);
            // 
            // EmotionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::LMA.Properties.Resources.desperate_2293377_1920;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonAddNote);
            this.Controls.Add(this.listViewEmotions);
            this.Controls.Add(this.buttonFetchDiary);
            this.Controls.Add(this.dateTimePickerEmotion);
            this.Name = "EmotionsForm";
            this.Text = "EmotionsForm";
            this.Load += new System.EventHandler(this.EmotionsForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateTimePickerEmotion;
        private System.Windows.Forms.Button buttonFetchDiary;
        private System.Windows.Forms.ListView listViewEmotions;
        private System.Windows.Forms.Button buttonAddNote;
    }
}
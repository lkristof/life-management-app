﻿namespace LMA.View
{
    partial class MeallAddForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dateTimePickerMealAdd = new System.Windows.Forms.DateTimePicker();
            this.textBoxMealName = new System.Windows.Forms.TextBox();
            this.checkedListBoxContainsMeal = new System.Windows.Forms.CheckedListBox();
            this.buttonAddGrocery = new System.Windows.Forms.Button();
            this.buttonAddMeal = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // dateTimePickerMealAdd
            // 
            this.dateTimePickerMealAdd.Location = new System.Drawing.Point(302, 35);
            this.dateTimePickerMealAdd.Name = "dateTimePickerMealAdd";
            this.dateTimePickerMealAdd.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerMealAdd.TabIndex = 0;
            // 
            // textBoxMealName
            // 
            this.textBoxMealName.Location = new System.Drawing.Point(302, 92);
            this.textBoxMealName.Name = "textBoxMealName";
            this.textBoxMealName.Size = new System.Drawing.Size(200, 20);
            this.textBoxMealName.TabIndex = 1;
            this.textBoxMealName.Text = "Name";
            // 
            // checkedListBoxContainsMeal
            // 
            this.checkedListBoxContainsMeal.FormattingEnabled = true;
            this.checkedListBoxContainsMeal.Location = new System.Drawing.Point(302, 148);
            this.checkedListBoxContainsMeal.Name = "checkedListBoxContainsMeal";
            this.checkedListBoxContainsMeal.Size = new System.Drawing.Size(200, 259);
            this.checkedListBoxContainsMeal.TabIndex = 2;
            // 
            // buttonAddGrocery
            // 
            this.buttonAddGrocery.Location = new System.Drawing.Point(593, 376);
            this.buttonAddGrocery.Name = "buttonAddGrocery";
            this.buttonAddGrocery.Size = new System.Drawing.Size(93, 30);
            this.buttonAddGrocery.TabIndex = 3;
            this.buttonAddGrocery.Text = "Add grocery";
            this.buttonAddGrocery.UseVisualStyleBackColor = true;
            this.buttonAddGrocery.Click += new System.EventHandler(this.buttonAddGrocery_Click);
            // 
            // buttonAddMeal
            // 
            this.buttonAddMeal.Location = new System.Drawing.Point(59, 380);
            this.buttonAddMeal.Name = "buttonAddMeal";
            this.buttonAddMeal.Size = new System.Drawing.Size(75, 23);
            this.buttonAddMeal.TabIndex = 4;
            this.buttonAddMeal.Text = "Add meal";
            this.buttonAddMeal.UseVisualStyleBackColor = true;
            this.buttonAddMeal.Click += new System.EventHandler(this.buttonAddMeal_Click);
            // 
            // MeallAddForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonAddMeal);
            this.Controls.Add(this.buttonAddGrocery);
            this.Controls.Add(this.checkedListBoxContainsMeal);
            this.Controls.Add(this.textBoxMealName);
            this.Controls.Add(this.dateTimePickerMealAdd);
            this.Name = "MeallAddForm";
            this.Text = "MeallAddForm";
            this.Load += new System.EventHandler(this.MeallAddForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateTimePickerMealAdd;
        private System.Windows.Forms.TextBox textBoxMealName;
        private System.Windows.Forms.CheckedListBox checkedListBoxContainsMeal;
        private System.Windows.Forms.Button buttonAddGrocery;
        private System.Windows.Forms.Button buttonAddMeal;
    }
}
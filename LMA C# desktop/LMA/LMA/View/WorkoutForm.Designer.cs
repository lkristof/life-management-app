﻿namespace LMA.View
{
    partial class WorkoutForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listViewWorkouts = new System.Windows.Forms.ListView();
            this.labelExerciseName = new System.Windows.Forms.Label();
            this.labelExerciseDescription = new System.Windows.Forms.Label();
            this.labelExerciseCategory = new System.Windows.Forms.Label();
            this.labelNoOfReps = new System.Windows.Forms.Label();
            this.labelYouTubeLink = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // listViewWorkouts
            // 
            this.listViewWorkouts.FullRowSelect = true;
            this.listViewWorkouts.GridLines = true;
            this.listViewWorkouts.Location = new System.Drawing.Point(13, 13);
            this.listViewWorkouts.Name = "listViewWorkouts";
            this.listViewWorkouts.Size = new System.Drawing.Size(179, 425);
            this.listViewWorkouts.TabIndex = 0;
            this.listViewWorkouts.UseCompatibleStateImageBehavior = false;
            this.listViewWorkouts.SelectedIndexChanged += new System.EventHandler(this.listViewWorkouts_SelectedIndexChanged);
            // 
            // labelExerciseName
            // 
            this.labelExerciseName.AutoSize = true;
            this.labelExerciseName.Location = new System.Drawing.Point(387, 47);
            this.labelExerciseName.Name = "labelExerciseName";
            this.labelExerciseName.Size = new System.Drawing.Size(76, 13);
            this.labelExerciseName.TabIndex = 1;
            this.labelExerciseName.Text = "Exercise name";
            // 
            // labelExerciseDescription
            // 
            this.labelExerciseDescription.AutoSize = true;
            this.labelExerciseDescription.Location = new System.Drawing.Point(387, 74);
            this.labelExerciseDescription.Name = "labelExerciseDescription";
            this.labelExerciseDescription.Size = new System.Drawing.Size(101, 13);
            this.labelExerciseDescription.TabIndex = 2;
            this.labelExerciseDescription.Text = "Exercise description";
            // 
            // labelExerciseCategory
            // 
            this.labelExerciseCategory.AutoSize = true;
            this.labelExerciseCategory.Location = new System.Drawing.Point(387, 114);
            this.labelExerciseCategory.Name = "labelExerciseCategory";
            this.labelExerciseCategory.Size = new System.Drawing.Size(91, 13);
            this.labelExerciseCategory.TabIndex = 3;
            this.labelExerciseCategory.Text = "Exercise category";
            // 
            // labelNoOfReps
            // 
            this.labelNoOfReps.AutoSize = true;
            this.labelNoOfReps.Location = new System.Drawing.Point(387, 150);
            this.labelNoOfReps.Name = "labelNoOfReps";
            this.labelNoOfReps.Size = new System.Drawing.Size(94, 13);
            this.labelNoOfReps.TabIndex = 4;
            this.labelNoOfReps.Text = "Exercise reps/time";
            // 
            // labelYouTubeLink
            // 
            this.labelYouTubeLink.AutoSize = true;
            this.labelYouTubeLink.Location = new System.Drawing.Point(387, 184);
            this.labelYouTubeLink.Name = "labelYouTubeLink";
            this.labelYouTubeLink.Size = new System.Drawing.Size(113, 13);
            this.labelYouTubeLink.TabIndex = 5;
            this.labelYouTubeLink.Text = "Exercise YouTube link";
            // 
            // WorkoutForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::LMA.Properties.Resources.crossfit_534615_1920;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.labelYouTubeLink);
            this.Controls.Add(this.labelNoOfReps);
            this.Controls.Add(this.labelExerciseCategory);
            this.Controls.Add(this.labelExerciseDescription);
            this.Controls.Add(this.labelExerciseName);
            this.Controls.Add(this.listViewWorkouts);
            this.Name = "WorkoutForm";
            this.Text = "WorkoutForm";
            this.Load += new System.EventHandler(this.WorkoutForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listViewWorkouts;
        private System.Windows.Forms.Label labelExerciseName;
        private System.Windows.Forms.Label labelExerciseDescription;
        private System.Windows.Forms.Label labelExerciseCategory;
        private System.Windows.Forms.Label labelNoOfReps;
        private System.Windows.Forms.Label labelYouTubeLink;
    }
}
﻿namespace LMA.View
{
    partial class FoodIntakeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.foodIntakeDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.buttonMealsFetch = new System.Windows.Forms.Button();
            this.listBoxMeals = new System.Windows.Forms.ListBox();
            this.buttonAddMeal = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // foodIntakeDateTimePicker
            // 
            this.foodIntakeDateTimePicker.Location = new System.Drawing.Point(13, 13);
            this.foodIntakeDateTimePicker.Name = "foodIntakeDateTimePicker";
            this.foodIntakeDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.foodIntakeDateTimePicker.TabIndex = 0;
            // 
            // buttonMealsFetch
            // 
            this.buttonMealsFetch.Location = new System.Drawing.Point(13, 56);
            this.buttonMealsFetch.Name = "buttonMealsFetch";
            this.buttonMealsFetch.Size = new System.Drawing.Size(200, 23);
            this.buttonMealsFetch.TabIndex = 1;
            this.buttonMealsFetch.Text = "Fetch data";
            this.buttonMealsFetch.UseVisualStyleBackColor = true;
            this.buttonMealsFetch.Click += new System.EventHandler(this.buttonMealsFetch_Click);
            // 
            // listBoxMeals
            // 
            this.listBoxMeals.FormattingEnabled = true;
            this.listBoxMeals.Location = new System.Drawing.Point(13, 103);
            this.listBoxMeals.Name = "listBoxMeals";
            this.listBoxMeals.Size = new System.Drawing.Size(288, 329);
            this.listBoxMeals.TabIndex = 2;
            // 
            // buttonAddMeal
            // 
            this.buttonAddMeal.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonAddMeal.Location = new System.Drawing.Point(527, 365);
            this.buttonAddMeal.Name = "buttonAddMeal";
            this.buttonAddMeal.Size = new System.Drawing.Size(126, 67);
            this.buttonAddMeal.TabIndex = 3;
            this.buttonAddMeal.Text = "Add meal";
            this.buttonAddMeal.UseVisualStyleBackColor = false;
            this.buttonAddMeal.Click += new System.EventHandler(this.buttonAddMeal_Click);
            // 
            // FoodIntakeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::LMA.Properties.Resources.food_1932466_1920;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonAddMeal);
            this.Controls.Add(this.listBoxMeals);
            this.Controls.Add(this.buttonMealsFetch);
            this.Controls.Add(this.foodIntakeDateTimePicker);
            this.Name = "FoodIntakeForm";
            this.Text = "FoodIntakeForm";
            this.Load += new System.EventHandler(this.FoodIntakeForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DateTimePicker foodIntakeDateTimePicker;
        private System.Windows.Forms.Button buttonMealsFetch;
        private System.Windows.Forms.ListBox listBoxMeals;
        private System.Windows.Forms.Button buttonAddMeal;
    }
}
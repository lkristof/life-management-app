﻿using LMA.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LMA
{
    public partial class PersonalAssistantForm : Form
    {
        public PersonalAssistantForm()
        {
            InitializeComponent();
            // Define the border style of the form to a dialog box.
            this.FormBorderStyle = FormBorderStyle.FixedDialog;

            // Set the MaximizeBox to false to remove the maximize box.
            this.MaximizeBox = false;

            // Set the MinimizeBox to false to remove the minimize box.
            this.MinimizeBox = false;

            // Set the start position of the form to the center of the screen.
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        private void PersonalAssistantForm_Load(object sender, EventArgs e)
        {

        }

        private void PersonalPlannerButton_Click(object sender, EventArgs e)
        {
            PersonalPlanner personalPlannerFrom = new PersonalPlanner();
            personalPlannerFrom.Show();
        }

        private void buttonSleepingTracker_Click(object sender, EventArgs e)
        {
            SleepingTrack sleepingTrack = new SleepingTrack();
            sleepingTrack.Show();
        }

        private void buttonNutritions_Click(object sender, EventArgs e)
        {
            FoodIntakeForm foodIntakeForm = new FoodIntakeForm();
            foodIntakeForm.Show();
        }

        private void buttonWorkoutAssistant_Click(object sender, EventArgs e)
        {
            WorkoutForm workoutForm = new WorkoutForm();
            workoutForm.Show();
        }
    }
}

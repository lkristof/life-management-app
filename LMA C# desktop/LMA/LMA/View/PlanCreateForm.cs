﻿using LMA.Controller;
using LMA.Controller.ControllerInterface;
using LMA.Model.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LMA.Resources
{
    public partial class PlanCreateForm : Form
    {
        ITaskController taskController;

        public PlanCreateForm(ITaskController taskController)
        {
            InitializeComponent();
            this.taskController = taskController;
        }

        private void buttonSavePlan_Click(object sender, EventArgs e)
        {
            taskController.addTask(this);
        }

        private void PlanCreateForm_Load(object sender, EventArgs e)
        {

        }
    }
}

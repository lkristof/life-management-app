﻿using LMA.Controller;
using LMA.Controller.ControllerInterface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LMA.View
{
    public partial class EmotionsForm : Form
    {
        IEmotionController emotionController;
        public EmotionsForm()
        {
            InitializeComponent();

            // Define the border style of the form to a dialog box.
            this.FormBorderStyle = FormBorderStyle.FixedDialog;

            // Set the MaximizeBox to false to remove the maximize box.
            this.MaximizeBox = false;

            // Set the MinimizeBox to false to remove the minimize box.
            this.MinimizeBox = false;
            emotionController = new EmotionController(this);
        }

        private void EmotionsForm_Load(object sender, EventArgs e)
        {

        }

        private void buttonAddNote_Click(object sender, EventArgs e)
        {
            AddNoteForm addNoteForm = new AddNoteForm(emotionController);
            addNoteForm.Show();
        }

        private void buttonFetchDiary_Click(object sender, EventArgs e)
        {
            emotionController.presentNotes();
        }
    }
}

﻿using LMA.Controller;
using LMA.Controller.ControllerInterface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LMA.View
{
    public partial class AddNoteForm : Form
    {
        IEmotionController emotionController;

        public AddNoteForm(IEmotionController emotionController)
        {
            InitializeComponent();

            // Define the border style of the form to a dialog box.
            this.FormBorderStyle = FormBorderStyle.FixedDialog;

            // Set the MaximizeBox to false to remove the maximize box.
            this.MaximizeBox = false;

            // Set the MinimizeBox to false to remove the minimize box.
            this.MinimizeBox = false;

            this.emotionController = emotionController;
        }

        private void AddNoteForm_Load(object sender, EventArgs e)
        {

        }

        private void buttonNoteSubmit_Click(object sender, EventArgs e)
        {
            emotionController.AddNote(this);
        }
    }
}

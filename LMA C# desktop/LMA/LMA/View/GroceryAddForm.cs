﻿using LMA.Controller;
using LMA.Controller.ControllerInterface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LMA.View
{
    public partial class GroceryAddForm : Form
    {
        IFoodController foodController;

        public GroceryAddForm(IFoodController foodController)
        {
            InitializeComponent();
            this.foodController = foodController;
        }

        private void GroceryAddForm_Load(object sender, EventArgs e)
        {
            
        }

        private void buttonSaveGrocery_Click(object sender, EventArgs e)
        {
            foodController.AddGrocery(this);
        }
    }
}

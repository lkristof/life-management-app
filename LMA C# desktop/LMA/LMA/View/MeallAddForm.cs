﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LMA.Controller;
using LMA.Controller.ControllerInterface;

namespace LMA.View
{
    public partial class MeallAddForm : Form
    {
        IFoodController foodController;

        public MeallAddForm(IFoodController foodController)
        {
            InitializeComponent();
            // Define the border style of the form to a dialog box.
            this.FormBorderStyle = FormBorderStyle.FixedDialog;

            // Set the MaximizeBox to false to remove the maximize box.
            this.MaximizeBox = false;

            // Set the MinimizeBox to false to remove the minimize box.
            this.MinimizeBox = false;
            this.foodController = foodController;
            foodController.initializeMealAddForm(this);
        }

        private void buttonAddGrocery_Click(object sender, EventArgs e)
        {
            GroceryAddForm groceryAddForm = new GroceryAddForm(foodController);
            groceryAddForm.Show();
        }

        private void MeallAddForm_Load(object sender, EventArgs e)
        {

        }

        private void buttonAddMeal_Click(object sender, EventArgs e)
        {
            foodController.AddMeal(this);
        }
    }
}

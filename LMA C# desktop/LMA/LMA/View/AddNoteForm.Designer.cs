﻿namespace LMA.View
{
    partial class AddNoteForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dateTimePickerNoteEmotion = new System.Windows.Forms.DateTimePicker();
            this.richTextBoxNoteEmotio = new System.Windows.Forms.RichTextBox();
            this.buttonNoteSubmit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // dateTimePickerNoteEmotion
            // 
            this.dateTimePickerNoteEmotion.Location = new System.Drawing.Point(308, 72);
            this.dateTimePickerNoteEmotion.Name = "dateTimePickerNoteEmotion";
            this.dateTimePickerNoteEmotion.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerNoteEmotion.TabIndex = 0;
            // 
            // richTextBoxNoteEmotio
            // 
            this.richTextBoxNoteEmotio.Location = new System.Drawing.Point(308, 114);
            this.richTextBoxNoteEmotio.Name = "richTextBoxNoteEmotio";
            this.richTextBoxNoteEmotio.Size = new System.Drawing.Size(200, 174);
            this.richTextBoxNoteEmotio.TabIndex = 1;
            this.richTextBoxNoteEmotio.Text = "";
            // 
            // buttonNoteSubmit
            // 
            this.buttonNoteSubmit.Location = new System.Drawing.Point(308, 314);
            this.buttonNoteSubmit.Name = "buttonNoteSubmit";
            this.buttonNoteSubmit.Size = new System.Drawing.Size(200, 51);
            this.buttonNoteSubmit.TabIndex = 2;
            this.buttonNoteSubmit.Text = "Add";
            this.buttonNoteSubmit.UseVisualStyleBackColor = true;
            this.buttonNoteSubmit.Click += new System.EventHandler(this.buttonNoteSubmit_Click);
            // 
            // AddNoteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonNoteSubmit);
            this.Controls.Add(this.richTextBoxNoteEmotio);
            this.Controls.Add(this.dateTimePickerNoteEmotion);
            this.Name = "AddNoteForm";
            this.Text = "AddNoteForm";
            this.Load += new System.EventHandler(this.AddNoteForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateTimePickerNoteEmotion;
        private System.Windows.Forms.RichTextBox richTextBoxNoteEmotio;
        private System.Windows.Forms.Button buttonNoteSubmit;
    }
}
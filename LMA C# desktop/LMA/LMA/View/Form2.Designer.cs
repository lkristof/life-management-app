﻿namespace LMA
{
    partial class PersonalAssistantForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PersonalPlannerButton = new System.Windows.Forms.Button();
            this.buttonSleepingTracker = new System.Windows.Forms.Button();
            this.buttonNutritions = new System.Windows.Forms.Button();
            this.buttonWorkoutAssistant = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // PersonalPlannerButton
            // 
            this.PersonalPlannerButton.Location = new System.Drawing.Point(42, 65);
            this.PersonalPlannerButton.Name = "PersonalPlannerButton";
            this.PersonalPlannerButton.Size = new System.Drawing.Size(208, 107);
            this.PersonalPlannerButton.TabIndex = 0;
            this.PersonalPlannerButton.Text = "Personal planner";
            this.PersonalPlannerButton.UseVisualStyleBackColor = true;
            this.PersonalPlannerButton.Click += new System.EventHandler(this.PersonalPlannerButton_Click);
            // 
            // buttonSleepingTracker
            // 
            this.buttonSleepingTracker.Location = new System.Drawing.Point(521, 65);
            this.buttonSleepingTracker.Name = "buttonSleepingTracker";
            this.buttonSleepingTracker.Size = new System.Drawing.Size(208, 107);
            this.buttonSleepingTracker.TabIndex = 1;
            this.buttonSleepingTracker.Text = "Sleeping tracker";
            this.buttonSleepingTracker.UseVisualStyleBackColor = true;
            this.buttonSleepingTracker.Click += new System.EventHandler(this.buttonSleepingTracker_Click);
            // 
            // buttonNutritions
            // 
            this.buttonNutritions.Location = new System.Drawing.Point(42, 287);
            this.buttonNutritions.Name = "buttonNutritions";
            this.buttonNutritions.Size = new System.Drawing.Size(208, 107);
            this.buttonNutritions.TabIndex = 2;
            this.buttonNutritions.Text = "Food intake tracker";
            this.buttonNutritions.UseVisualStyleBackColor = true;
            this.buttonNutritions.Click += new System.EventHandler(this.buttonNutritions_Click);
            // 
            // buttonWorkoutAssistant
            // 
            this.buttonWorkoutAssistant.Location = new System.Drawing.Point(521, 287);
            this.buttonWorkoutAssistant.Name = "buttonWorkoutAssistant";
            this.buttonWorkoutAssistant.Size = new System.Drawing.Size(208, 107);
            this.buttonWorkoutAssistant.TabIndex = 3;
            this.buttonWorkoutAssistant.Text = "Workout assistant";
            this.buttonWorkoutAssistant.UseVisualStyleBackColor = true;
            this.buttonWorkoutAssistant.Click += new System.EventHandler(this.buttonWorkoutAssistant_Click);
            // 
            // PersonalAssistantForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(160)))), ((int)(((byte)(77)))));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonWorkoutAssistant);
            this.Controls.Add(this.buttonNutritions);
            this.Controls.Add(this.buttonSleepingTracker);
            this.Controls.Add(this.PersonalPlannerButton);
            this.Name = "PersonalAssistantForm";
            this.Text = "Personal assistant";
            this.Load += new System.EventHandler(this.PersonalAssistantForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button PersonalPlannerButton;
        private System.Windows.Forms.Button buttonSleepingTracker;
        private System.Windows.Forms.Button buttonNutritions;
        private System.Windows.Forms.Button buttonWorkoutAssistant;
    }
}
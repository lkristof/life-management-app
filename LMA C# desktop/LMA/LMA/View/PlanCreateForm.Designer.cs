﻿namespace LMA.Resources
{
    partial class PlanCreateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dateTimePickerPlanAdding = new System.Windows.Forms.DateTimePicker();
            this.PlanNameTextBox = new System.Windows.Forms.TextBox();
            this.TextBoxPlanDesc = new System.Windows.Forms.RichTextBox();
            this.buttonSavePlan = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // dateTimePickerPlanAdding
            // 
            this.dateTimePickerPlanAdding.Location = new System.Drawing.Point(290, 41);
            this.dateTimePickerPlanAdding.Name = "dateTimePickerPlanAdding";
            this.dateTimePickerPlanAdding.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerPlanAdding.TabIndex = 0;
            // 
            // PlanNameTextBox
            // 
            this.PlanNameTextBox.Location = new System.Drawing.Point(290, 101);
            this.PlanNameTextBox.Name = "PlanNameTextBox";
            this.PlanNameTextBox.Size = new System.Drawing.Size(200, 20);
            this.PlanNameTextBox.TabIndex = 1;
            // 
            // TextBoxPlanDesc
            // 
            this.TextBoxPlanDesc.Location = new System.Drawing.Point(290, 176);
            this.TextBoxPlanDesc.Name = "TextBoxPlanDesc";
            this.TextBoxPlanDesc.Size = new System.Drawing.Size(200, 95);
            this.TextBoxPlanDesc.TabIndex = 2;
            this.TextBoxPlanDesc.Text = "";
            // 
            // buttonSavePlan
            // 
            this.buttonSavePlan.Location = new System.Drawing.Point(290, 310);
            this.buttonSavePlan.Name = "buttonSavePlan";
            this.buttonSavePlan.Size = new System.Drawing.Size(200, 19);
            this.buttonSavePlan.TabIndex = 3;
            this.buttonSavePlan.Text = "Save";
            this.buttonSavePlan.UseVisualStyleBackColor = true;
            this.buttonSavePlan.Click += new System.EventHandler(this.buttonSavePlan_Click);
            // 
            // PlanCreateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonSavePlan);
            this.Controls.Add(this.TextBoxPlanDesc);
            this.Controls.Add(this.PlanNameTextBox);
            this.Controls.Add(this.dateTimePickerPlanAdding);
            this.Name = "PlanCreateForm";
            this.Text = "PlanCreateForm";
            this.Load += new System.EventHandler(this.PlanCreateForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateTimePickerPlanAdding;
        private System.Windows.Forms.TextBox PlanNameTextBox;
        private System.Windows.Forms.RichTextBox TextBoxPlanDesc;
        private System.Windows.Forms.Button buttonSavePlan;
    }
}
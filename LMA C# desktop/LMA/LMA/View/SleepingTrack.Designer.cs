﻿namespace LMA.View
{
    partial class SleepingTrack
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxSleeping = new System.Windows.Forms.ListBox();
            this.dateTimePickerSleep = new System.Windows.Forms.DateTimePicker();
            this.buttonFetchSleep = new System.Windows.Forms.Button();
            this.textBoxSleepStart = new System.Windows.Forms.TextBox();
            this.textBoxSleepEnd = new System.Windows.Forms.TextBox();
            this.textBoxRatingSleep = new System.Windows.Forms.TextBox();
            this.buttonSaveSleep = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listBoxSleeping
            // 
            this.listBoxSleeping.FormattingEnabled = true;
            this.listBoxSleeping.Location = new System.Drawing.Point(12, 151);
            this.listBoxSleeping.Name = "listBoxSleeping";
            this.listBoxSleeping.Size = new System.Drawing.Size(220, 225);
            this.listBoxSleeping.TabIndex = 0;
            // 
            // dateTimePickerSleep
            // 
            this.dateTimePickerSleep.Location = new System.Drawing.Point(13, 33);
            this.dateTimePickerSleep.Name = "dateTimePickerSleep";
            this.dateTimePickerSleep.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerSleep.TabIndex = 1;
            // 
            // buttonFetchSleep
            // 
            this.buttonFetchSleep.Location = new System.Drawing.Point(13, 92);
            this.buttonFetchSleep.Name = "buttonFetchSleep";
            this.buttonFetchSleep.Size = new System.Drawing.Size(200, 23);
            this.buttonFetchSleep.TabIndex = 2;
            this.buttonFetchSleep.Text = "Fetch";
            this.buttonFetchSleep.UseVisualStyleBackColor = true;
            this.buttonFetchSleep.Click += new System.EventHandler(this.buttonFetchSleep_Click);
            // 
            // textBoxSleepStart
            // 
            this.textBoxSleepStart.Location = new System.Drawing.Point(688, 47);
            this.textBoxSleepStart.Name = "textBoxSleepStart";
            this.textBoxSleepStart.Size = new System.Drawing.Size(100, 20);
            this.textBoxSleepStart.TabIndex = 4;
            this.textBoxSleepStart.Text = "Sleep start";
            // 
            // textBoxSleepEnd
            // 
            this.textBoxSleepEnd.Location = new System.Drawing.Point(688, 92);
            this.textBoxSleepEnd.Name = "textBoxSleepEnd";
            this.textBoxSleepEnd.Size = new System.Drawing.Size(100, 20);
            this.textBoxSleepEnd.TabIndex = 5;
            this.textBoxSleepEnd.Text = "Sleep end";
            // 
            // textBoxRatingSleep
            // 
            this.textBoxRatingSleep.Location = new System.Drawing.Point(688, 138);
            this.textBoxRatingSleep.Name = "textBoxRatingSleep";
            this.textBoxRatingSleep.Size = new System.Drawing.Size(100, 20);
            this.textBoxRatingSleep.TabIndex = 6;
            this.textBoxRatingSleep.Text = "Rating (1 to 5)";
            // 
            // buttonSaveSleep
            // 
            this.buttonSaveSleep.Location = new System.Drawing.Point(688, 182);
            this.buttonSaveSleep.Name = "buttonSaveSleep";
            this.buttonSaveSleep.Size = new System.Drawing.Size(100, 23);
            this.buttonSaveSleep.TabIndex = 7;
            this.buttonSaveSleep.Text = "Save";
            this.buttonSaveSleep.UseVisualStyleBackColor = true;
            this.buttonSaveSleep.Click += new System.EventHandler(this.buttonSaveSleep_Click);
            // 
            // SleepingTrack
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::LMA.Properties.Resources.fox_1284512_1920;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonSaveSleep);
            this.Controls.Add(this.textBoxRatingSleep);
            this.Controls.Add(this.textBoxSleepEnd);
            this.Controls.Add(this.textBoxSleepStart);
            this.Controls.Add(this.buttonFetchSleep);
            this.Controls.Add(this.dateTimePickerSleep);
            this.Controls.Add(this.listBoxSleeping);
            this.Name = "SleepingTrack";
            this.Text = "SleepingTrack";
            this.Load += new System.EventHandler(this.SleepingTrack_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxSleeping;
        private System.Windows.Forms.DateTimePicker dateTimePickerSleep;
        private System.Windows.Forms.Button buttonFetchSleep;
        private System.Windows.Forms.TextBox textBoxSleepStart;
        private System.Windows.Forms.TextBox textBoxSleepEnd;
        private System.Windows.Forms.TextBox textBoxRatingSleep;
        private System.Windows.Forms.Button buttonSaveSleep;
    }
}
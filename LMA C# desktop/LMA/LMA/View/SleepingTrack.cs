﻿using LMA.Controller;
using LMA.Controller.ControllerInterface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LMA.View
{
    public partial class SleepingTrack : Form
    {
        ISleepController sleepController;

        public SleepingTrack()
        {
            InitializeComponent();
            // Define the border style of the form to a dialog box.
            this.FormBorderStyle = FormBorderStyle.FixedDialog;

            // Set the MaximizeBox to false to remove the maximize box.
            this.MaximizeBox = false;

            // Set the MinimizeBox to false to remove the minimize box.
            this.MinimizeBox = false;

            sleepController = new SleepController(this);
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void SleepingTrack_Load(object sender, EventArgs e)
        {

        }

        private void buttonFetchSleep_Click(object sender, EventArgs e)
        {
            sleepController.fetchSleep();
        }

        private void buttonSaveSleep_Click(object sender, EventArgs e)
        {
            sleepController.storeSleep();
        }
    }
}

﻿namespace LMA
{
    partial class PersonalPlanner
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PlansDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.NewPlanButton = new System.Windows.Forms.Button();
            this.PlansExtractButton = new System.Windows.Forms.Button();
            this.PlansListBox1 = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // PlansDateTimePicker
            // 
            this.PlansDateTimePicker.Location = new System.Drawing.Point(25, 27);
            this.PlansDateTimePicker.Name = "PlansDateTimePicker";
            this.PlansDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.PlansDateTimePicker.TabIndex = 0;
            // 
            // NewPlanButton
            // 
            this.NewPlanButton.BackColor = System.Drawing.SystemColors.ControlDark;
            this.NewPlanButton.Location = new System.Drawing.Point(595, 46);
            this.NewPlanButton.Name = "NewPlanButton";
            this.NewPlanButton.Size = new System.Drawing.Size(165, 79);
            this.NewPlanButton.TabIndex = 1;
            this.NewPlanButton.Text = "Add new plan";
            this.NewPlanButton.UseVisualStyleBackColor = false;
            this.NewPlanButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // PlansExtractButton
            // 
            this.PlansExtractButton.Location = new System.Drawing.Point(25, 67);
            this.PlansExtractButton.Name = "PlansExtractButton";
            this.PlansExtractButton.Size = new System.Drawing.Size(200, 23);
            this.PlansExtractButton.TabIndex = 2;
            this.PlansExtractButton.Text = "Extract plans";
            this.PlansExtractButton.UseVisualStyleBackColor = true;
            this.PlansExtractButton.Click += new System.EventHandler(this.PlansExtractButton_Click);
            // 
            // PlansListBox1
            // 
            this.PlansListBox1.FormattingEnabled = true;
            this.PlansListBox1.Location = new System.Drawing.Point(25, 125);
            this.PlansListBox1.Name = "PlansListBox1";
            this.PlansListBox1.Size = new System.Drawing.Size(395, 290);
            this.PlansListBox1.TabIndex = 3;
            // 
            // PersonalPlanner
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::LMA.Properties.Resources.arm_1284248_1920;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.PlansListBox1);
            this.Controls.Add(this.PlansExtractButton);
            this.Controls.Add(this.NewPlanButton);
            this.Controls.Add(this.PlansDateTimePicker);
            this.Name = "PersonalPlanner";
            this.Text = "PersonalPlanner";
            this.Load += new System.EventHandler(this.PersonalPlanner_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DateTimePicker PlansDateTimePicker;
        private System.Windows.Forms.Button NewPlanButton;
        private System.Windows.Forms.Button PlansExtractButton;
        private System.Windows.Forms.ListBox PlansListBox1;
    }
}
﻿using LMA.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LMA
{
    public partial class LMA : Form
    {
        public LMA()
        {
            InitializeComponent();
            // Define the border style of the form to a dialog box.
            this.FormBorderStyle = FormBorderStyle.FixedDialog;

            // Set the MaximizeBox to false to remove the maximize box.
            this.MaximizeBox = false;

            // Set the MinimizeBox to false to remove the minimize box.
            this.MinimizeBox = false;

            // Set the start position of the form to the center of the screen.
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            PersonalAssistantForm personalAssistantForm = new PersonalAssistantForm();
            personalAssistantForm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            EmotionsForm emotionsForm = new EmotionsForm();
            emotionsForm.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MotivationsForm motivationsForm = new MotivationsForm();
            motivationsForm.Show();
        }
    }
}

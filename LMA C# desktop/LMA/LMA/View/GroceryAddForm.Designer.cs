﻿namespace LMA.View
{
    partial class GroceryAddForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxGroceryName = new System.Windows.Forms.TextBox();
            this.textBoxGroceryAmount = new System.Windows.Forms.TextBox();
            this.textBoxCalories = new System.Windows.Forms.TextBox();
            this.textBoxFats = new System.Windows.Forms.TextBox();
            this.textBoxCarbs = new System.Windows.Forms.TextBox();
            this.textBoxProteins = new System.Windows.Forms.TextBox();
            this.buttonSaveGrocery = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBoxGroceryName
            // 
            this.textBoxGroceryName.Location = new System.Drawing.Point(300, 44);
            this.textBoxGroceryName.Name = "textBoxGroceryName";
            this.textBoxGroceryName.Size = new System.Drawing.Size(170, 20);
            this.textBoxGroceryName.TabIndex = 0;
            this.textBoxGroceryName.Text = "Name";
            // 
            // textBoxGroceryAmount
            // 
            this.textBoxGroceryAmount.Location = new System.Drawing.Point(300, 89);
            this.textBoxGroceryAmount.Name = "textBoxGroceryAmount";
            this.textBoxGroceryAmount.Size = new System.Drawing.Size(170, 20);
            this.textBoxGroceryAmount.TabIndex = 1;
            this.textBoxGroceryAmount.Text = "Amount";
            // 
            // textBoxCalories
            // 
            this.textBoxCalories.Location = new System.Drawing.Point(300, 135);
            this.textBoxCalories.Name = "textBoxCalories";
            this.textBoxCalories.Size = new System.Drawing.Size(170, 20);
            this.textBoxCalories.TabIndex = 2;
            this.textBoxCalories.Text = "Calories";
            // 
            // textBoxFats
            // 
            this.textBoxFats.Location = new System.Drawing.Point(300, 185);
            this.textBoxFats.Name = "textBoxFats";
            this.textBoxFats.Size = new System.Drawing.Size(170, 20);
            this.textBoxFats.TabIndex = 3;
            this.textBoxFats.Text = "Fats";
            // 
            // textBoxCarbs
            // 
            this.textBoxCarbs.Location = new System.Drawing.Point(300, 233);
            this.textBoxCarbs.Name = "textBoxCarbs";
            this.textBoxCarbs.Size = new System.Drawing.Size(170, 20);
            this.textBoxCarbs.TabIndex = 4;
            this.textBoxCarbs.Text = "Carbs";
            // 
            // textBoxProteins
            // 
            this.textBoxProteins.Location = new System.Drawing.Point(300, 279);
            this.textBoxProteins.Name = "textBoxProteins";
            this.textBoxProteins.Size = new System.Drawing.Size(170, 20);
            this.textBoxProteins.TabIndex = 5;
            this.textBoxProteins.Text = "Proteins";
            // 
            // buttonSaveGrocery
            // 
            this.buttonSaveGrocery.Location = new System.Drawing.Point(300, 322);
            this.buttonSaveGrocery.Name = "buttonSaveGrocery";
            this.buttonSaveGrocery.Size = new System.Drawing.Size(170, 66);
            this.buttonSaveGrocery.TabIndex = 6;
            this.buttonSaveGrocery.Text = "Save";
            this.buttonSaveGrocery.UseVisualStyleBackColor = true;
            this.buttonSaveGrocery.Click += new System.EventHandler(this.buttonSaveGrocery_Click);
            // 
            // GroceryAddForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonSaveGrocery);
            this.Controls.Add(this.textBoxProteins);
            this.Controls.Add(this.textBoxCarbs);
            this.Controls.Add(this.textBoxFats);
            this.Controls.Add(this.textBoxCalories);
            this.Controls.Add(this.textBoxGroceryAmount);
            this.Controls.Add(this.textBoxGroceryName);
            this.Name = "GroceryAddForm";
            this.Text = "GroceryAddForm";
            this.Load += new System.EventHandler(this.GroceryAddForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxGroceryName;
        private System.Windows.Forms.TextBox textBoxGroceryAmount;
        private System.Windows.Forms.TextBox textBoxCalories;
        private System.Windows.Forms.TextBox textBoxFats;
        private System.Windows.Forms.TextBox textBoxCarbs;
        private System.Windows.Forms.TextBox textBoxProteins;
        private System.Windows.Forms.Button buttonSaveGrocery;
    }
}
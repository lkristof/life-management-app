﻿using LMA.Controller.ControllerInterface;
using LMA.Model;
using LMA.Model.Repository;
using LMA.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LMA.Controller
{
    public class EmotionController : IEmotionController
    {
        EmotionsForm emotionsForm;
        NoteRepository noteRepository;

        public EmotionController(EmotionsForm emotionsForm)
        {
            this.emotionsForm = emotionsForm;
            noteRepository = new NoteRepository(NHibernateService.OpenSession());
        }

        public void presentNotes()
        {
            ListView listView = (ListView)emotionsForm.Controls["listViewEmotions"];
            listView.Clear();
            listView.View = System.Windows.Forms.View.Details;
            listView.Columns.Add("Note", 160);
            DateTimePicker dateTimePicker = (DateTimePicker)emotionsForm.Controls["dateTimePickerEmotion"];
            List<Note> listOfNotes = noteRepository.getNotesByDate(dateTimePicker.Value);
            foreach (Note note in listOfNotes)
            {
                string[] row = { note.description };
                ListViewItem listViewItem = new ListViewItem(row);
                listViewItem.Text = note.description;
                listView.Items.Add(listViewItem);
            }

        }

        public void AddNote(AddNoteForm addNoteForm)
        {
            DateTimePicker dateTimePicker = (DateTimePicker)addNoteForm.Controls["dateTimePickerNoteEmotion"];
            RichTextBox richTextBox = (RichTextBox)addNoteForm.Controls["richTextBoxNoteEmotio"];
            Note note = new Note(dateTimePicker.Value, richTextBox.Text);
            noteRepository.addNote(note);
            addNoteForm.Close();
        }
    }
}

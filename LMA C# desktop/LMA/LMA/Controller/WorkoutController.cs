﻿using LMA.Controller.ControllerInterface;
using LMA.Model;
using LMA.Model.Repository;
using LMA.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LMA.Controller
{
    public class WorkoutController : IWorkoutController
    {
        WorkoutForm workoutForm;
        WorkoutRepository workoutRepository;

        public WorkoutController(WorkoutForm workoutForm)
        {
            this.workoutForm = workoutForm;
            workoutRepository = new WorkoutRepository(NHibernateService.OpenSession());
        }

        public void initializeForm()
        {
            ListView listView = (ListView)workoutForm.Controls["listViewWorkouts"];
            listView.View = System.Windows.Forms.View.Details;
            listView.Columns.Add("name", 60);
            List<Workout> listOfWorkouts = workoutRepository.getAllWorkouts(); 
            foreach(Workout workout in listOfWorkouts)
            {
                string[] row = { workout.name };
                ListViewItem listViewItem = new ListViewItem(row);
                listViewItem.Text = workout.name;
                listView.Items.Add(listViewItem);
            }
        }

        public void itemSelected()
        {
            ListView listView = (ListView)workoutForm.Controls["listViewWorkouts"];
            if (listView.SelectedItems.Count == 0)
                return;
            var firstSelectedItem = listView.SelectedItems[0];
            string workoutName = firstSelectedItem.Text;
            List<Workout> listOfWorkouts = workoutRepository.getAllWorkouts();
            Workout selectedWorkout = null;
            foreach (Workout workout in listOfWorkouts)
                if (workout.name.Equals(workoutName))
                    selectedWorkout = workout;
            Label labelName = (Label)workoutForm.Controls["labelExerciseName"];
            labelName.Text = "Name: " + selectedWorkout.name;
            Label labelDescription = (Label)workoutForm.Controls["labelExerciseDescription"];
            labelDescription.Text = "Description: " + selectedWorkout.description;
            Label labelCategory = (Label)workoutForm.Controls["labelExerciseCategory"];
            labelCategory.Text = "Category: " + selectedWorkout.category;
            Label labelReps = (Label)workoutForm.Controls["labelNoOfReps"];
            labelReps.Text = "Reps: " + selectedWorkout.reps.ToString();
            Label labelYouTubeLink = (Label)workoutForm.Controls["labelYouTubeLink"];
            labelYouTubeLink.Text = "Link: " + selectedWorkout.link;
        }
    }
}

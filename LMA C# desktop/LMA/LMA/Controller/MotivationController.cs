﻿using LMA.Controller.ControllerInterface;
using LMA.Model;
using LMA.Model.Repository;
using LMA.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LMA.Controller
{
    public class MotivationController : IMotivationController
    {
        MotivationsForm motivationsForm;
        MotivationRepository motivationRepository;
        MotivationCategoryRepository motivationCategoryRepository;

        public MotivationController(MotivationsForm motivationsForm)
        {
            this.motivationsForm = motivationsForm;
            motivationRepository = new MotivationRepository(NHibernateService.OpenSession());
            motivationCategoryRepository = new MotivationCategoryRepository(NHibernateService.OpenSession());
        }

        public void initForm()
        {
            ComboBox comboBox = (ComboBox)motivationsForm.Controls["comboBoxCategory"];
            List<MotivationCategory> listOfMotivationCategories = motivationCategoryRepository.getAllMotivations();
            foreach (MotivationCategory motivationCategory in listOfMotivationCategories)
                comboBox.Items.Add(motivationCategory);
        }

        public void onFetch()
        {
            ComboBox comboBox = (ComboBox)motivationsForm.Controls["comboBoxCategory"];
            MotivationCategory motivationCategory = (MotivationCategory)comboBox.SelectedItem;
            List<Motivation> listOfMotivations = motivationRepository.getMotivationsByCategory(motivationCategory);
            CheckedListBox checkedListBox = (CheckedListBox)motivationsForm.Controls["checkedListBoxMotivation"];
            checkedListBox.DataSource = listOfMotivations;
        }

        public void dislike()
        {
            CheckedListBox checkedList = (CheckedListBox)motivationsForm.Controls["checkedListBoxMotivation"];
            List<Motivation> listOfGroceries = new List<Motivation>();
            foreach (Motivation motivation in checkedList.CheckedItems)
            {
                motivationRepository.deleteMotivation(motivation);
            }
        }
    }
}

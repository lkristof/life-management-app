﻿using LMA.Controller.ControllerInterface;
using LMA.Model.Repository;
using LMA.Model.RepositoryInterface;
using LMA.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LMA.Controller
{
    public class TaskController : ITaskController
    {
        PersonalPlanner personalPlanner;
        ITaskRepository taskRepository;

        public TaskController(PersonalPlanner personalPlanner)
        {
            this.personalPlanner = personalPlanner;
            taskRepository = new TaskRepository(NHibernateService.OpenSession());
        }

        public void addTask(PlanCreateForm planCreateForm)
        {
            DateTimePicker dateTimePicker = (DateTimePicker) planCreateForm.Controls["dateTimePickerPlanAdding"];
            TextBox textBox = (TextBox)planCreateForm.Controls["PlanNameTextBox"];
            RichTextBox richTextBox = (RichTextBox)planCreateForm.Controls["TextBoxPlanDesc"];
            Model.Task task = new Model.Task(textBox.Text, dateTimePicker.Value, richTextBox.Text);
            taskRepository.addTask(task);
            planCreateForm.Close();
        }

        public void presentTasks()
        {
            ListBox listView = (ListBox)personalPlanner.Controls.Find("PlansListBox1", true).First();
            DateTimePicker datePicker = (DateTimePicker)personalPlanner.Controls.Find("PlansDateTimePicker", true).First();
            var myItems = taskRepository.getTasksByDate(datePicker.Value);
            var stringItems = new List<string>();
            foreach (Model.Task t in myItems)
                stringItems.Add(t.ToString());
            listView.DataSource = stringItems;
            listView.HorizontalScrollbar = true;
        }
    }
}

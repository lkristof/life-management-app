﻿using LMA.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Controller.ControllerInterface
{
    public interface IFoodController
    {
        void fetchFoodByDate();
        void initializeMealAddForm(MeallAddForm meallAddForm);
        void AddGrocery(GroceryAddForm groceryAddForm);
        void AddMeal(MeallAddForm meallAddForm);
    }
}

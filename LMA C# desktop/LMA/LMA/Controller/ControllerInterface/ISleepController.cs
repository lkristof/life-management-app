﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Controller.ControllerInterface
{
    public interface ISleepController
    {
        void fetchSleep();
        void storeSleep();
        bool validate(string text1, string text2, string text3);
    }
}

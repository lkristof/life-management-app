﻿using LMA.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Controller.ControllerInterface
{
    public interface ITaskController
    {
        void addTask(PlanCreateForm planCreateForm);
        void presentTasks();
    }
}

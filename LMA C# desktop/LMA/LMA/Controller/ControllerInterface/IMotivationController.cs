﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Controller.ControllerInterface
{
    public interface IMotivationController
    {
        void initForm();
        void onFetch();
        void dislike();
    }
}

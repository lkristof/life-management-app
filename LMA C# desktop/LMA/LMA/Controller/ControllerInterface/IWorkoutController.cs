﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Controller.ControllerInterface
{
    public interface IWorkoutController
    {
        void itemSelected();
        void initializeForm();
    }
}

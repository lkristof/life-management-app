﻿using LMA.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMA.Controller.ControllerInterface
{
    public interface IEmotionController
    {
        void presentNotes();
        void AddNote(AddNoteForm addNoteForm);
    }
}

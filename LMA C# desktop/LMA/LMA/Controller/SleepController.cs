﻿using LMA.Controller.ControllerInterface;
using LMA.Model;
using LMA.Model.Repository;
using LMA.Model.RepositoryInterface;
using LMA.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LMA.Controller
{
    public class SleepController : ISleepController
    {
        SleepingTrack sleepingTrack;
        ISleepRepository sleepRepository;

        public SleepController(SleepingTrack sleepingTrack)
        {
            this.sleepingTrack = sleepingTrack;
            sleepRepository = new SleepRepository(NHibernateService.OpenSession());
        }

        public void fetchSleep()
        {
            ListBox listView = (ListBox)sleepingTrack.Controls.Find("listBoxSleeping", true).First();
            DateTimePicker datePicker = (DateTimePicker)sleepingTrack.Controls.Find("dateTimePickerSleep", true).First();
            Sleep sleep = sleepRepository.getSleepByDate(datePicker.Value);
            if (sleep == null)
            {
                return;
            }
                
            var list = new List<Sleep>();
            list.Add(sleep);
            listView.DataSource = list;
            listView.HorizontalScrollbar = true;
        }

        public void storeSleep()
        {
            TextBox textBoxSleepStart = (TextBox)sleepingTrack.Controls["textBoxSleepStart"];
            TextBox textBoxSleepEnd = (TextBox)sleepingTrack.Controls["textBoxSleepEnd"];
            TextBox textBoxSleepRating = (TextBox)sleepingTrack.Controls["textBoxRatingSleep"];
            RichTextBox textBoxSleepDescription = (RichTextBox)sleepingTrack.Controls["richTextBoxSleepDescription"];
            if (!validate(textBoxSleepStart.Text, textBoxSleepEnd.Text, textBoxSleepRating.Text))
            {
                MessageBox.Show("Please enter a valid rating");
                return;
            }
            Sleep sleep = new Sleep(DateTime.Now, textBoxSleepStart.Text, textBoxSleepEnd.Text, int.Parse(textBoxSleepRating.Text));
            sleepRepository.addSleep(sleep);
        }

        public bool validate(string text1, string text2, string text3)
        {
            try
            {
                int.Parse(text3);
                return true;
            } catch (Exception e)
            {
                return false;
            }
        }
    }
}

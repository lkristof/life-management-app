﻿using LMA.Controller.ControllerInterface;
using LMA.Model;
using LMA.Model.Repository;
using LMA.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LMA.Controller
{
    public class FoodController : IFoodController
    {
        FoodIntakeForm foodIntakeForm;
        MealRepository mealRepository;
        GroceryRepository groceryRepository;

        public FoodController(FoodIntakeForm foodIntakeForm)
        {
            this.foodIntakeForm = foodIntakeForm;
            mealRepository = new MealRepository(NHibernateService.OpenSession());
            groceryRepository = new GroceryRepository(NHibernateService.OpenSession());
        }

        public void fetchFoodByDate()
        {
            DateTimePicker dateTimePicker = (DateTimePicker)foodIntakeForm.Controls["foodIntakeDateTimePicker"];
            List<Meal> listOfMeals = mealRepository.getMealsByDate(dateTimePicker.Value);
            ListBox listBoxMeals = (ListBox)foodIntakeForm.Controls["listBoxMeals"];
            listBoxMeals.DataSource = listOfMeals;
        }

        public void initializeMealAddForm(MeallAddForm meallAddForm)
        {
            List<Grocery> listOfGroceries = groceryRepository.getAllGroceries();
            CheckedListBox checkedList = (CheckedListBox)meallAddForm.Controls["checkedListBoxContainsMeal"];
            checkedList.DataSource = listOfGroceries;
        }

        public void AddGrocery(GroceryAddForm groceryAddForm)
        {
            TextBox textBoxName = (TextBox)groceryAddForm.Controls["textBoxGroceryName"];
            TextBox textBoxAmount = (TextBox)groceryAddForm.Controls["textBoxGroceryAmount"];
            TextBox textBoxCalories = (TextBox)groceryAddForm.Controls["textBoxCalories"];
            TextBox textBoxFats = (TextBox)groceryAddForm.Controls["textBoxFats"];
            TextBox textBoxCarbs = (TextBox)groceryAddForm.Controls["textBoxCarbs"];
            TextBox textBoxProteins = (TextBox)groceryAddForm.Controls["textBoxProteins"];

            double amount = double.Parse(textBoxAmount.Text);
            double calories = double.Parse(textBoxCalories.Text);
            double fats = double.Parse(textBoxFats.Text);
            double carbs = double.Parse(textBoxCarbs.Text);
            double proteins = double.Parse(textBoxProteins.Text);

            Grocery grocery = new Grocery(textBoxName.Text, amount, calories, fats, carbs, proteins);
            groceryRepository.addGrocery(grocery);

            groceryAddForm.Close();
        }

        public void AddMeal(MeallAddForm meallAddForm)
        {
            DateTimePicker dateTimePicker = (DateTimePicker)meallAddForm.Controls["dateTimePickerMealAdd"];
            TextBox textBox = (TextBox)meallAddForm.Controls["textBoxMealName"];
            CheckedListBox checkedList = (CheckedListBox)meallAddForm.Controls["checkedListBoxContainsMeal"];
            List<Grocery> listOfGroceries = new List<Grocery>();
            foreach(Grocery grocery in checkedList.CheckedItems)
            {
                listOfGroceries.Add(grocery);
            }
            Meal meal = new Meal(dateTimePicker.Value, textBox.Text, listOfGroceries);
            mealRepository.addMeal(meal);
            meallAddForm.Close();
        }
    }
}
